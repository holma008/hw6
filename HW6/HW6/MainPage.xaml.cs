﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;
using Entry = Microcharts.Entry;

namespace HW6
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            CreateChart();
        }
       private void CreateChart()
        {
             var entryList = new List<Entry>()
                {
                   new Entry(328)
                      {
                         Label = "Label 1",
                         Color = SKColor.Parse("#FF4523"),
                      },
                   new Entry(189)
                      {
                         Label = "Label 2",
                         Color = SKColor.Parse("#445676"),
                      },
                   new Entry(464)
                     {
                        Label = "Label 3",
                        Color = SKColor.Parse("#662345"),
                     },
                };
            var barChart = new BarChart() { Entries = entryList };
            var donutChart = new DonutChart() { Entries = entryList };
            var lineChart = new LineChart() { Entries = entryList };
            Chart1.Chart = barChart;
            Chart2.Chart = donutChart;
            Chart3.Chart = lineChart;
        }
        private void ChartPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            string selectedChart = (string)picker.SelectedItem;

            if(selectedChart == "Bar Chart")
            {
                Chart2.IsVisible = false;
                Chart3.IsVisible = false;
                Chart1.IsVisible = true;
            }
            if (selectedChart == "Donut Chart")
            {
                Chart1.IsVisible = false;
                Chart3.IsVisible = false;
                Chart2.IsVisible = true;
            }
            if (selectedChart == "Line Chart")
            {
                Chart2.IsVisible = false;
                Chart1.IsVisible = false;
                Chart3.IsVisible = true;
            }
        }
    }
}
